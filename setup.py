from setuptools import setup


setup(
    name = "EAC",
    version = "0.1",
    packages = ["EAC"],

    author = "Yeison Cardona",
    author_email = "yeisoneng@gmail.com",
    maintainer = "Yeison Cardona",
    maintainer_email = "yeisoneng@gmail.com",

    #url = "http://www.pinguino.cc/",
    url = "http://yeisoncardona.com/",
    download_url = "https://bitbucket.org/yeisoneng/python-eac/downloads",


    #include_package_data=True,
    license = "BSD License",
    description = "Enhanced Autocorrelation",
    # long_description = README,
    #author = "Yeison Cardona",
    #author_email = "yeisoneng@gmail.com",
    #classifiers = [
        #"Environment :: Web Environment",
        #"Framework :: Django",
    #],
)
